﻿using UnityEngine;

public class TransformManipulationBox : MonoBehaviour
{
    [SerializeField]
    private Transform encompassedTransform;

    public GameObject target;
    public GameObject basis;

    public GameObject bottom;
    public GameObject bottom_forward;
    public GameObject bottom_right;
    public GameObject bottom_forward_right;

    public GameObject top;
    public GameObject top_forward;
    public GameObject top_right;
    public GameObject top_forward_right;

    public GameObject middle;
    public GameObject middle_forward;
    public GameObject middle_right;
    public GameObject middle_forward_right;

    public GameObject bottom_midright_forward;
    public GameObject bottom_midforward;
    public GameObject bottom_midright;
    public GameObject bottom_midforward_right;

    public GameObject top_midright_forward;
    public GameObject top_midforward;
    public GameObject top_midright;
    public GameObject top_midforward_right;

    public GameObject tube_middle;
    public GameObject tube_middle_forward;
    public GameObject tube_middle_right;
    public GameObject tube_middle_forward_right;

    public GameObject tube_bottom_midright_forward;
    public GameObject tube_bottom_midforward;
    public GameObject tube_bottom_midright;
    public GameObject tube_bottom_midforward_right;

    public GameObject tube_top_midright_forward;
    public GameObject tube_top_midforward;
    public GameObject tube_top_midright;
    public GameObject tube_top_midforward_right;



    public GameObject movehandler;

    Renderer ren;


    void Start()
    {
        ren = target.GetComponent<Renderer>();
    }

    void Update()
    {
        
        Vector3 min = ren.bounds.min;
        Vector3 max = ren.bounds.max;
        Vector3 half = ren.bounds.center;



        bottom.transform.position = ren.bounds.min;
        top.transform.position = ren.bounds.max;

        bottom_forward.transform.position = new Vector3(min.x, min.y, max.z);
        bottom_forward_right.transform.position = new Vector3(max.x, min.y, max.z);
        bottom_right.transform.position = new Vector3(max.x, min.y, min.z);

        top_forward.transform.position = new Vector3(min.x, max.y, max.z);
        top_forward_right.transform.position = new Vector3(min.x, max.y, min.z);
        top_right.transform.position = new Vector3(max.x, max.y, min.z);

        middle.transform.position = new Vector3(min.x, half.y, min.z);
        tube_middle.transform.position = middle.transform.position;

        middle_forward.transform.position = new Vector3(min.x, half.y, max.z);
        tube_middle_forward.transform.position = middle_forward.transform.position;
        
        middle_right.transform.position = new Vector3(max.x, half.y, min.z);
        tube_middle_right.transform.position = middle_right.transform.position;
        
        middle_forward_right.transform.position = new Vector3(max.x, half.y, max.z);
        tube_middle_forward_right.transform.position = middle_forward_right.transform.position; 

        bottom_midright_forward.transform.position = new Vector3(min.x, min.y, half.z);
        tube_bottom_midright_forward.transform.position = top_midforward_right.transform.position;
        
        bottom_midforward.transform.position = new Vector3(min.x, max.y, half.z);
        tube_bottom_midforward.transform.position = bottom_midforward.transform.position;

        bottom_midright.transform.position = new Vector3(max.x, min.y, half.z);
        tube_bottom_midright.transform.position = top_midforward.transform.position;
        
        bottom_midforward_right.transform.position = new Vector3(max.x, max.y, half.z);
        tube_bottom_midforward_right.transform.position = bottom_midforward_right.transform.position;


        top_midright_forward.transform.position = new Vector3(half.x, min.y, min.z);
        tube_top_midright_forward.transform.position = top_midright_forward.transform.position;

        top_midforward.transform.position = new Vector3(half.x, min.y, max.z);
        tube_top_midforward.transform.position = bottom_midright.transform.position;
        
        top_midright.transform.position = new Vector3(half.x, max.y, min.z);
        tube_top_midright.transform.position  = top_midright.transform.position;
        
        top_midforward_right.transform.position = new Vector3(half.x, max.y, max.z);
        tube_top_midforward_right.transform.position = bottom_midright_forward.transform.position;


    }
}
