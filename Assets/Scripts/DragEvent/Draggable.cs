﻿using UnityEngine;

public class Draggable : MonoBehaviour
{
    public event DragEventHandler OnDrag;

    public virtual void Drag(DragEventArgs eventArgs)
    {
        OnDrag.Invoke(this, eventArgs);
    }
}